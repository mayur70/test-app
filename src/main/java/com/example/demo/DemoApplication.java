package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public void myMethod(boolean a, int b, int c, char d, String e, float f){
		if(false){
			System.out.println("Hello");
		}

		if (a){
			if (b == 0){
				if (c == 1){
					System.out.println("Hello");
				} else if(d == 'd'){
					System.out.println("Hello");

					if(f == 0.3){
						System.out.println("Hello");
					}
				}
			}
		}
	}

	public void MyMethod33_get(boolean a, int b, int c, char d, String e, float f){
		if(false){
			System.out.println("Hello");
		}

		if (a){
			if (b == 0){
				if (c == 1){
					System.out.println("Hello");
				} else if(d == 'd'){
					System.out.println("Hello");

					if(f == 0.3){
						System.out.println("Hello");
					}
				}
			}
		}
	}

	public void MyMethod22_get(boolean a, int b, int c, char d, String e, float f){
		if(false){
			return;
		}

		if (a){
			if (b == 0){
				if (c == 1){
					//TODO: do something
					//System.out.println("Hello");
				} else if(d == 'd'){
					//System.out.println("Hello");

					//if(f == 0.3){
					//	System.out.println("Hello");
					//}
				}
			}
		}
	}
}
